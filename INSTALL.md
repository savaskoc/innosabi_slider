# Slider Assignment
Since you are using Angular, and I don't have any experince with Angular 2+, I decided write this project with that. So, this is my first Angular 2+ project.
## Installation
You can run `npm install` to install this project. After that, you can serve with `npm run start`.
## Tests
After installation, you can run the tests with `npm run test`.
