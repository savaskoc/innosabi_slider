import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {of} from 'rxjs';

import {SliderComponent} from './slider.component';
import {SliderService} from '../../services/slider/slider.service';
import {SliderJsonService} from '../../services/slider/slider-json.service';

describe('SliderComponent', () => {
  let sliderService: SliderService;
  let component: SliderComponent;
  let fixture: ComponentFixture<SliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SliderComponent],
      imports: [NgbCarouselModule],
      providers: [{provide: SliderService, useClass: SliderJsonService}]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderComponent);
    component = fixture.componentInstance;
    sliderService = TestBed.inject(SliderService);

    spyOn(sliderService, 'getSlides').and.returnValue(of([{
      image: 'https://via.placeholder.com/150',
      title: 'Sample Title',
      description: 'Sample Description'
    }]));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render slide', () => {
    const compiled = fixture.nativeElement;
    const carouselItem = compiled.querySelector('.carousel-item');
    const carouselImage = carouselItem.querySelector('img');

    expect(carouselImage.src).toEqual('https://via.placeholder.com/150');
    expect(carouselImage.alt).toEqual('Sample Title');
    expect(carouselItem.querySelector('article h1').textContent).toEqual('Sample Title');
    expect(carouselItem.querySelector('article p').textContent).toEqual('Sample Description');
  });
});
