import {Component, OnInit} from '@angular/core';
import {EMPTY, Observable} from 'rxjs';
import {Slide, SliderService} from '../../services/slider/slider.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  slides$: Observable<Slide[]> = EMPTY;

  constructor(protected sliderService: SliderService) {
  }

  ngOnInit(): void {
    this.slides$ = this.sliderService.getSlides(window.innerWidth, window.innerHeight);
  }
}
