import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {MockComponent} from 'ng-mocks';
import {SliderComponent} from './components/slider/slider.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockComponent(SliderComponent)
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
