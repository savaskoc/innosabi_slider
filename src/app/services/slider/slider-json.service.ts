import {Injectable} from '@angular/core';
import {Slide, SliderService} from './slider.service';
import * as projects from './projects.json';
import {environment} from '../../../environments/environment';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SliderJsonService implements SliderService {
  protected data: Slide[];

  constructor() {
    this.data = projects.data.map(({image, name: title, description}) => ({
      image: `${environment.imageUrl}/api/v4/media/${image}/thumbnail`,
      title,
      description
    }));
  }

  getSlides(width: number, height: number): Observable<Slide[]> {
    return of(this.data.map(({image, ...rest}) => ({
      image: `${image}/width/${width}/height/${height}/strategy/crop`,
      ...rest
    })));
  }
}
