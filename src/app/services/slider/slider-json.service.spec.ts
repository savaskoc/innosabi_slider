import { TestBed } from '@angular/core/testing';

import { SliderJsonService } from './slider-json.service';

describe('SliderJsonService', () => {
  let service: SliderJsonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SliderJsonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
