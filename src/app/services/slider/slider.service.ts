import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

export interface Slide {
  image: string;
  title: string;
  description: string;
}

@Injectable({
  providedIn: 'root'
})
export abstract class SliderService {
  abstract getSlides(width: number, height: number): Observable<Slide[]>;
}
