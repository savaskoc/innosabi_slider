import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {SliderComponent} from './components/slider/slider.component';
import {SliderService} from './services/slider/slider.service';
import {SliderJsonService} from './services/slider/slider-json.service';

@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
  ],
  imports: [
    BrowserModule,
    NgbCarouselModule,
  ],
  providers: [{provide: SliderService, useClass: SliderJsonService}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
